<?php

namespace App\Exceptions;

class LoginException extends \Exception
{

    public function report()
    {

    }

    public function render($request)
    {
            return response(
                [
                    'message' => $this->getMessage(),
                ], 401
            );
    }

}
