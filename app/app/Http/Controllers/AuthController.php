<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login', 'register']]);
    }


    public function register()
    {
        $this->validate(request(), User::rules(false));
        return response()->json(
            User::create(request()->toArray())
        );
    }

    public function login()
    {
        $this->validate(request(), User::rules(true));
        return response()->json(
            User::getToken(
                request(['email', 'password'])
            )
        );
    }

    public function me()
    {
        return response()->json(
            auth()->user()
        );
    }

    public function logout()
    {
        auth()->logout();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function refresh()
    {
        return response()->json(
            User::refresh()
        );
    }

}
