<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Jenssegers\Mongodb\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Arr;

class User extends Model implements AuthorizableContract, AuthenticatableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    protected $hidden = [
        'password',
    ];

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public static function rules(bool $is_login = false, User $user = null)
    {
        $rules = [
            'name' => ['string', 'max:255', 'required'],
            'email' => ['string', 'email', 'required', $is_login ? '' : 'unique:users', 'max:255'],
            'password' => ['string', 'max:255', 'min:6', 'required', $is_login ? '' : 'confirmed'],
        ];

        return $is_login ? Arr::only($rules, ['email', 'password']) : $rules;

    }

    public static function getToken($credentials)
    {
        if (! $token = auth()->attempt($credentials)) {
            throw new \App\Exceptions\LoginException('Unauthorized');
        }
        return self::prepareToken($token);
    }

    public static function refreshToken()
    {
        return self::prepareToken(auth()->refresh());
    }

    protected static function prepareToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);
    }
}
