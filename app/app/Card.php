<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
       "title", "description", "status", "type",
    ];

    public const TYPES = [
        0 => "Player",
        1 => "Effect",
        1 => "Action",
        2 => "Interception",
    ];

}
