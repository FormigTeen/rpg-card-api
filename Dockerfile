FROM php:fpm-alpine

LABEL maintainer="Mahmoud Zalt <mahmoud@zalt.me>"

ENV PHP_VERSION=7.4.6 \
    APP_NAME=RpgCard \
    APP_ENV=production \
    APP_DEBUG=true \
    APP_URL=rpgcard.test \
    DB_HOST=localhost \
    DB_PORT=27017 \
    DB_DATABASE=rpgcard \
    DB_USERNAME=rpgcard \
    DB_PASSWORD=rpgcard \
    QUEUE_CONNECTION=database \
    CACHE_DRIVER=file

COPY ./app /var/www

# always run apt update when start and after add new source list, then clean up at end.
RUN set -xe; \
        apk add --update --no-cache --virtual \
        build-dependencies build-base openssl-dev autoconf \
        pcre-dev ${PHPIZE_DEPS} \
        libzip-dev zip unzip && \
        docker-php-ext-configure zip && \
        docker-php-ext-install zip && \
        pecl install mongodb && \
        docker-php-ext-enable mongodb && \
        pecl config-set php_ini /etc/php.ini && \
        apk del build-dependencies build-base openssl-dev autoconf && \
        rm -rf /var/cache/apk/*

USER root

###########################################################################
# Install Composer
###########################################################################

RUN cd /var/www && \
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
        php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
        php composer-setup.php && \
        php -r "unlink('composer-setup.php');" && \
        php composer.phar install --no-dev && \
        cp .env.example .env && \
        php artisan key:generate

# Configure locale.
ARG LOCALE=POSIX
ENV LC_ALL ${LOCALE}

WORKDIR /var/www

VOLUME /var/www

CMD ["php-fpm"]

EXPOSE 9000
